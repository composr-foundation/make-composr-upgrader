<?php

if (php_sapi_name() != 'cli') {
    exit('Run from command line');
}

if (!isset($_SERVER['argv'][2])) {
    exit('Syntax: php make_composr_upgrader.php <git_clone_path> <addon>[ <addon>...]' . "\n");
}

if (strpos(shell_exec('tar --help'), 'Usage: tar') === false) {
    exit('tar not found' . "\n");
}

$path = $_SERVER['argv'][1];
if (!is_file($path . '/sources/version.php')) {
    exit('Could not find Git clone' . "\n");
}

$addons = array();
foreach ($_SERVER['argv'] as $i => $arg) {
    if ($i < 2) {
        continue;
    }

    $arg = trim($arg);

    if (empty($arg)) {
        continue;
    }

    if (strpos($arg, '.') !== false) {
        if (substr($arg, -4) != '.php') {
            continue;
        }

        $arg = substr($arg, 0, strlen($arg) - 4);
    }

    $addon_path = $path . '/sources_custom/hooks/systems/addon_registry/' . $arg . '.php';
    if (!is_file($addon_path)) {
        $addon_path = $path . '/sources/hooks/systems/addon_registry/' . $arg . '.php';
    }
    if (!is_file($addon_path)) {
        echo 'Warning: Could not find addon file, ' . $addon_path . "\n";
        continue;
    }

    $addons[$arg] = $addon_path;
}

$all_files = array();

foreach ($addons as $addon => $addon_path) {
    require_once($addon_path);
    $hook_class = 'Hook_addon_registry_' . $addon;
    $ob = new $hook_class;
    $files = $ob->get_file_list();
    foreach ($files as $file) {
        if ($file == 'info.php') { // ocPortal
            continue;
        }
        if ($file == '_config.php') {
            continue;
        }

        if (!is_file($path . '/' . $file)) { // ocPortal
            if (preg_match('#^\w+\.tpl$#', $file) != 0) {
                $file = 'themes/default/templates/' . $file;
            }
            if (preg_match('#^\w+\.css$#', $file) != 0) {
                $file = 'themes/default/templates/' . $file;
            }
        }
        $all_files[] = $file;
    }
}

file_put_contents('tar_list.txt', implode("\n", $all_files));

$cwd = getcwd();
chdir($path);
echo shell_exec('tar -cvf ' . escapeshellarg($cwd . '/custom_upgrade.tar') . ' -T ' . escapeshellarg($cwd . '/tar_list.txt'));
chdir($cwd);

if (!is_file('custom_upgrade.tar')) {
    exit('Failed to build custom_upgrade.tar' . "\n");
}

echo 'Successfully created custom_upgrade.tar, now extract it to your Composr site that needs to be upgraded' . "\n";
