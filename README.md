This is a simple script to build a custom extractable upgrader for Composr from a Git clone.
It is useful if the compo.sr infrastructure is not available for some reason, or the site being upgraded is severely broken in some way and cannot run the upgrader.
Composr comes with a large number of bundled addons, any of which may be uninstalled. For this reason upgrading Composr is not as straight-forward as just extracting an archive - having files return from uninstalled addons is a big problem and must be avoided.
This script also works with ocPortal (you can consider this the same as Composr for the purpose of this script).

This script is not designed as a general-purpose Composr upgrader. There is inbuilt tooling, and tooling on compo.sr, for normal upgrading situations.

The script is designed to be run on a Linux machine (which can be your server if you can open an SSH session).
At minimum it needs GNU Tar to be available in the path.
These instructions assume Git is installed, although it is not necessary if you manually download and extract the repositories.

Usage is as follows...

1) Clone the Composr or ocPortal repository:

Composr:
```
git clone git@gitlab.com:composr-foundation/composr.git
cd composr
pwd
```

ocPortal (legacy):
```
git@github.com:chrisgraham/ocPortal.git
cd ocPortal
git checkout --track origin/v9_urgent_maintenance
pwd
cd ..
```

It will tell you the path of the Composr-clone. You'll need it in step 3 (`/path/to/clone`).

2) Clone this repository and enter directory:
```
git clone git@gitlab.com:composr-foundation/make-composr-upgrader.git
cd make-composr-upgrader
pwd
```

It will tell you the path of the make-composr-upgrader clone.

3) Work out what addons your old site has by looking at your outdated Composr installation:

Run this on your server:
```
ls -1 /path/to/website/sources/hooks/systems/addon_registry | tr "\n" " "
```

You should get something like this:
```
actionlog.php aggregate_types.php apache_config_files.php authors.php awards.php backup.php banners.php bookmarks.php breadcrumbs.php calendar.php captcha.php catalogues.php chat.php cns_avatars.php cns_cartoon_avatars.php cns_clubs.php cns_contact_member.php cns_cpfs.php cns_forum.php cns_member_avatars.php cns_member_photos.php cns_member_titles.php cns_multi_moderations.php cns_post_templates.php cns_reported_posts.php cns_signatures.php cns_thematic_avatars.php cns_warnings.php code_editor.php collaboration_zone.php commandr.php content_privacy.php content_reviews.php core_abstract_components.php core_abstract_interfaces.php core_addon_management.php core_adminzone_dashboard.php core_cleanup_tools.php core_cns.php core_comcode_pages.php core_configuration.php core_database_drivers.php core_feedback_features.php core_fields.php core_form_interfaces.php core_forum_drivers.php core_graphic_text.php core_html_abstractions.php core_language_editing.php core_menus.php core_notifications.php core_permission_management.php core.php core_primary_layout.php core_rich_media.php core_themeing.php core_upgrader.php core_webstandards.php core_zone_editor.php counting_blocks.php custom_comcode.php debrand.php downloads.php ecommerce.php errorlog.php failover.php filedump.php forum_blocks.php galleries.php google_appengine.php help_page.php hphp_buildkit.php import.php index.html installer.php language_block.php ldap.php linux_helper_scripts.php match_key_permissions.php msn.php newsletter.php news.php news_shared.php page_management.php phpinfo.php points.php pointstore.php polls.php printer_friendly_block.php quizzes.php random_quotes.php realtime_rain.php recommend.php redirects_editor.php rootkit_detector.php search.php securitylogging.php setupwizard.php shopping.php sms.php ssl.php staff_messaging.php staff.php stats_block.php stats.php supermember_directory.php syndication_blocks.php syndication.php textbased_persistent_caching.php themewizard.php tickets.php uninstaller.php unvalidated.php users_online_block.php welcome_emails.php wiki.php windows_helper_scripts.php wordfilter.php xml_fields.php zone_logos.php
```

4) Run the script to create an upgrade TAR file from the Git clone of step 1:
```
php make_composr_upgrader.php /path/to/composr_clone actionlog.php aggregate_types.php apache_config_files.php authors.php awards.php backup.php banners.php bookmarks.php breadcrumbs.php calendar.php captcha.php catalogues.php chat.php cns_avatars.php cns_cartoon_avatars.php cns_clubs.php cns_contact_member.php cns_cpfs.php cns_forum.php cns_member_avatars.php cns_member_photos.php cns_member_titles.php cns_multi_moderations.php cns_post_templates.php cns_reported_posts.php cns_signatures.php cns_thematic_avatars.php cns_warnings.php code_editor.php collaboration_zone.php commandr.php content_privacy.php content_reviews.php core_abstract_components.php core_abstract_interfaces.php core_addon_management.php core_adminzone_dashboard.php core_cleanup_tools.php core_cns.php core_comcode_pages.php core_configuration.php core_database_drivers.php core_feedback_features.php core_fields.php core_form_interfaces.php core_forum_drivers.php core_graphic_text.php core_html_abstractions.php core_language_editing.php core_menus.php core_notifications.php core_permission_management.php core.php core_primary_layout.php core_rich_media.php core_themeing.php core_upgrader.php core_webstandards.php core_zone_editor.php counting_blocks.php custom_comcode.php debrand.php downloads.php ecommerce.php errorlog.php failover.php filedump.php forum_blocks.php galleries.php google_appengine.php help_page.php hphp_buildkit.php import.php index.html installer.php language_block.php ldap.php linux_helper_scripts.php match_key_permissions.php msn.php newsletter.php news.php news_shared.php page_management.php phpinfo.php points.php pointstore.php polls.php printer_friendly_block.php quizzes.php random_quotes.php realtime_rain.php recommend.php redirects_editor.php rootkit_detector.php search.php securitylogging.php setupwizard.php shopping.php sms.php ssl.php staff_messaging.php staff.php stats_block.php stats.php supermember_directory.php syndication_blocks.php syndication.php textbased_persistent_caching.php themewizard.php tickets.php uninstaller.php unvalidated.php users_online_block.php welcome_emails.php wiki.php windows_helper_scripts.php wordfilter.php xml_fields.php zone_logos.php
```

`custom_upgrade.tar` should have been created. This is a regular extractable TAR archive made just for you to directly extract, not the regular kind of Composr upgrader TAR that gets fed through the Composr upgrader.

5) Take the `custom_upgrade.tar` file that was generated and extract it

Assuming that your site is on the same server as you ran this script on...

```
cd /path/to/composr_clone
tar xvf /path/to/make-composr-upgrader_clone/custom_upgrade.tar
```

If this assumption is not true, you'll need to upload the TAR to your server and extract it to your outdated site by other means.
